-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Jan-2019 às 12:13
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `estacionamento`
--
CREATE DATABASE IF NOT EXISTS `estacionamento` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `estacionamento`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `placa` varchar(10) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `telefone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `placa`, `nome`, `telefone`) VALUES
(4, 'dada', 'caio gabriel', '123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `registros`
--

CREATE TABLE `registros` (
  `id` int(11) NOT NULL,
  `placa` varchar(10) NOT NULL,
  `cliente` varchar(50) DEFAULT NULL,
  `dataDeEntrada` date DEFAULT NULL,
  `horaDeEntrada` time DEFAULT NULL,
  `usuario` int(11) DEFAULT NULL,
  `presente` tinyint(1) DEFAULT NULL,
  `horaDeSaida` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `registros`
--

INSERT INTO `registros` (`id`, `placa`, `cliente`, `dataDeEntrada`, `horaDeEntrada`, `usuario`, `presente`, `horaDeSaida`) VALUES
(1, 'hjr-0302', 'Maria', '2018-12-10', '01:30:51', 2, NULL, NULL),
(2, 'gma-2230', 'João', '2018-12-10', '01:35:57', 2, NULL, NULL),
(3, 'raw-8822', 'João', '2018-12-10', '01:45:50', 2, 0, NULL),
(4, 'rwa-8899', 'pedro', '2018-12-10', '01:58:42', 2, 0, NULL),
(5, 'tha-7000', 'Gustavo', '2018-12-10', '02:02:06', 2, 0, NULL),
(6, 'ama-8888', 'Ashley', '2018-12-10', '02:03:39', 2, 0, NULL),
(7, 'uij-7766', 'teste', '2018-12-10', '02:08:04', 2, 0, NULL),
(8, 'tes-7843', 'teste2', '2018-12-10', '14:08:59', 2, 0, NULL),
(9, 'gas-9966', 'Amaral', '2018-12-10', '14:14:09', 2, 0, NULL),
(10, 'ad74889', 'gilmar', '2018-12-10', '11:15:30', 2, 0, NULL),
(11, 'fas-5855', 'ff', '2019-01-08', '06:05:44', 2, 0, NULL),
(12, 'fas-5864', '', '2019-01-08', '06:18:42', 2, 0, NULL),
(13, 'faf5a49', '', '2019-01-08', '07:17:05', 2, 0, NULL),
(14, 'fadsf', '', '2019-01-08', '19:19:00', 2, 0, NULL),
(15, '454fd8', '', '2019-01-08', '19:20:53', 2, 0, NULL),
(16, 'kjij55', '', '2019-01-08', '19:21:42', 2, 0, NULL),
(17, 'lkjk', '', '2019-01-08', '19:22:04', 2, 0, NULL),
(18, 'fd', '', '2019-01-08', '20:35:12', 2, 0, NULL),
(19, 'uij-7766  ', '', '2019-01-10', '18:43:44', 1, 0, NULL),
(20, 'uij-7766  ', '', '2019-01-10', '18:43:47', 1, 0, NULL),
(21, 'uij-7766  ', '', '2019-01-10', '18:43:49', 1, 0, NULL),
(22, 'uij-7766  ', '', '2019-01-10', '18:43:50', 1, 0, NULL),
(23, 'ggg-4545', 'caio', '2019-01-14', '09:43:23', 1, 0, NULL),
(24, 'ggg-4545', 'caio', '2019-01-14', '09:43:28', 1, 0, NULL),
(25, 'tda-4648', '', '2019-01-14', '09:43:48', 1, 1, NULL),
(26, 'tda-4648', '', '2019-01-14', '09:43:51', 1, 1, NULL),
(27, 'tyty-4646', 'gtgt', '2019-01-14', '09:49:05', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `registros_finalizados`
--

CREATE TABLE `registros_finalizados` (
  `id` int(11) NOT NULL,
  `placa` varchar(10) NOT NULL,
  `valor` float NOT NULL,
  `horarioDeEntrada` time NOT NULL,
  `horarioDeSaida` time NOT NULL,
  `dataDeEntrada` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `registros_finalizados`
--

INSERT INTO `registros_finalizados` (`id`, `placa`, `valor`, `horarioDeEntrada`, `horarioDeSaida`, `dataDeEntrada`) VALUES
(1, 'ama-8888', 117, '02:03:39', '21:34:12', '2018-12-10'),
(2, 'uij-7766', 99.5, '02:08:04', '18:43:26', '2018-12-10'),
(3, 'uij-7766%2', 0.1, '18:43:44', '18:45:01', '2019-01-10'),
(4, 'uij-7766%2', 0.1, '18:43:47', '18:45:05', '2019-01-10'),
(5, 'uij-7766%2', 0.1, '18:43:49', '18:45:08', '2019-01-10'),
(6, 'uij-7766%2', 0.1, '18:43:50', '18:45:10', '2019-01-10'),
(7, 'tes-7843', 29, '14:08:59', '18:59:03', '2018-12-10'),
(8, 'gas-9966', 28.4, '14:14:09', '18:59:05', '2018-12-10'),
(9, 'ad74889', 46.3, '11:15:30', '18:59:06', '2018-12-10'),
(10, 'fas-5855', 77.3, '06:05:44', '18:59:08', '2019-01-08'),
(11, 'fas-5864', 76, '06:18:42', '18:59:13', '2019-01-08'),
(12, 'faf5a49', 70.2, '07:17:05', '18:59:14', '2019-01-08'),
(13, '454fd8', 2.1, '19:20:53', '18:59:16', '2019-01-08'),
(14, 'kjij55', 2.2, '19:21:42', '18:59:17', '2019-01-08'),
(15, 'lkjk', 2.2, '19:22:04', '18:59:19', '2019-01-08'),
(16, 'fd', 9.5, '20:35:12', '18:59:25', '2019-01-08'),
(17, 'ggg-4545', 0.5, '09:43:23', '09:49:11', '2019-01-14'),
(18, 'ggg-4545', 0.6, '09:43:28', '09:50:08', '2019-01-14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`) VALUES
(1, 'admin', 'admin', '202cb962ac59075b964b07152d234b70'),
(2, 'gustavo', 'gustavo', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registros`
--
ALTER TABLE `registros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario` (`usuario`);

--
-- Indexes for table `registros_finalizados`
--
ALTER TABLE `registros_finalizados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `registros`
--
ALTER TABLE `registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `registros_finalizados`
--
ALTER TABLE `registros_finalizados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `registros`
--
ALTER TABLE `registros`
  ADD CONSTRAINT `registros_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
