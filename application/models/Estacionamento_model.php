<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estacionamento_model extends CI_Model {
	
	public function logar($usuario=NULL, $senha=NULL){
		if($usuario != NULL and $senha != NULL){
			$query = $this->db->query("SELECT * FROM usuarios WHERE usuario = '$usuario' and senha = md5('$senha')");
			$this->db->limit(1);
			return $query->row();
		}
		
	}

	public function getUsuario($usuario=NULL){
		if($usuario != NULL){
			$this->db->where('usuario',$usuario);
			$this->db->limit(1);
			$query = $this->db->get('usuarios');
			return $query->row();
		}
	}

	public function adicionarUsuario($nome, $usuario, $senha){
		$this->db->query("INSERT INTO Usuarios(nome, usuario, senha) VALUES ('$nome', '$usuario', md5('$senha'))");
		$_SESSION['erro'] = "Novo usuário cadastrado com sucesso!";
	}

	public function adicionarEntrada($placa=NULL, $cliente
		){
		date_default_timezone_set("America/Sao_Paulo");
		if($placa != NULL){
			$this->db->insert('registros', array(
				'cliente'=>$cliente,
				'placa'=>$placa,
				'usuario'=>$_SESSION['id'],
				'dataDeEntrada'=>date("Y-m-d"),
				'horaDeEntrada'=>date("H:i:s"),
				'presente'=>1
				));
		}
	}

	public function adicionarRegistroFinalizado($placa=NULL, $valor, $horaDeEntrada, $horaDeSaida, $dataDeEntrada){
		if($placa != NULL){
			$this->db->insert('registros_finalizados', array(
				'placa'=>$placa,
				'valor'=>$valor,
				'horarioDeEntrada'=>$horaDeEntrada,
				'horarioDeSaida'=>$horaDeSaida,
				'dataDeEntrada'=>$dataDeEntrada
				));
		}
	}

	public function getRegistros(){
		$query = $this->db->query("SELECT * FROM registros WHERE presente=1");
		return $query->result();
	}

	public function PesquisarPlaca($placa){
		$this->db->where('placa',$placa);
		$query = $this->db->get('registros_finalizados');
		return $query->result();
	}

	public function getClientes(){
		$query = $this->db->query("SELECT * FROM clientes");
		return $query->result();
	}

	public function getRegistroById($id){
		$this->db->where('id',$id);
		$this->db->limit(1);
		$query = $this->db->get('registros');
		return $query->row();
	}

	public function updateRegistro($id){
		$this->db->query("UPDATE registros SET presente = 0 WHERE id=$id");
	}

	public function deletaCliente($id){
		$this->db->query("DELETE FROM clientes WHERE id=$id ");
	}

	public function getUnicoCliente($id){
		$query = $this->db->query("SELECT * FROM clientes WHERE id=$id");
		$this->db->limit(1);
		return $query->row();
	}

	public function updateCliente($nome,$placa,$telefone,$id){
		$query = $this->db->query("UPDATE clientes SET nome = '$nome',placa= '$placa',telefone ='$telefone' WHERE id=$id");
	}

	public function insertCliente($nome, $placa, $telefone){
		 $this->db->insert('clientes', array(
			'placa'=>$placa,
			'nome'=>$nome,
			'telefone'=>$telefone
			));
	}
}