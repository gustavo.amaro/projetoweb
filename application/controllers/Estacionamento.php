<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estacionamento extends CI_Controller {
	//abre a view home

	public function __construct(){
		parent::__construct();
		//carrega o model estacionamento_model setando o apelido estacionamento
		$this->load->model('estacionamento_model','estacionamento');
		//carrega a library session que permite o uso de seções
		$this->load->library('session');
	}
	public function index(){
		if(!isset($_SESSION['nome'])){
			$this->load->view('entre');
		}else{
			$this->load->view('home');
		}
	}
	//realiza o login do usuário e abre a pagina home do sistema, se usuario e senha estiverem corretos.
	public function home(){
		$usuario = $this->input->post('usuario');
		$senha = $this->input->post('senha');
		$result = $this->estacionamento->logar($usuario, $senha);
		if(isset($result)){
			$_SESSION['nome'] = $result->nome;
			$_SESSION['usuario'] = $result->usuario;
			$_SESSION['id'] = $result->id;
			$_SESSION['erro'] = "";
			$this->load->view('home');
		}else{
			$_SESSION['erro'] = "Usuário e/ou senha incorretos!";
			header('location: /Estacionamento');
		}
	}

	public function registrarSaida($id){
		date_default_timezone_set("America/Sao_Paulo");
		$registro = $this->estacionamento->getRegistroById($id);
		$dados['placa'] = $registro->placa;
		$dados['dataDeEntrada'] = $registro->dataDeEntrada;
		$dados['horaDeEntrada'] = $registro->horaDeEntrada;
		$dados['cliente'] = $registro->cliente;
		$horaDeEntrada = new DateTime(date($registro->horaDeEntrada));
		$horaDeSaida = new DateTime(date("H:i:s"));
		$diferencaHorario = $horaDeSaida->diff($horaDeEntrada)->format('%H:%I:%S');
		$horario = explode(':',$diferencaHorario);
		$valor = ($horario[0]*60 + $horario[1])*0.1;
		$dados['valor'] = $valor;
		$dados['horaDeSaida'] = date('H:i:s');
		$dados['id'] = $registro->id;
		$this->load->view("RegistrarSaida", $dados);
	}

	//realiza o logoff do usuário e retorna a pagina de login.
	public function sair(){
		unset($_SESSION['nome'], $_SESSION['usuario'], $_SESSION['id']);
		header('location: /Estacionamento');
	}

	public function confirmaSaida($id, $placa, $valor, $horaDeEntrada, $horaDeSaida, $dataDeEntrada){
		$this->estacionamento->updateRegistro($id);
		$this->estacionamento->adicionarRegistroFinalizado($placa, $valor, $horaDeEntrada, $horaDeSaida, $dataDeEntrada);
		header('location: /Estacionamento');
	}

	public function addUsuario(){
		$nome = $this->input->post('nome');
		$usuario = $this->input->post('usuario');
		$senha = $this->input->post('senha');
		$senha2 = $this->input->post('senha2');

		$existe = $this->estacionamento->getUsuario($usuario);
		if(!empty($existe)){
			$_SESSION['erro'] = "Esse nome de usuário já está sendo utilizado!";
			header('location: /Estacionamento/adicionarUsuario');
		}else{
			if($senha == $senha2){
			$this->estacionamento->adicionarUsuario($nome, $usuario, $senha);
			header('location: /Estacionamento/adicionarUsuario');
			}else{
				$_SESSION['erro'] = "As senhas não correspondem!";
				header('location: /Estacionamento/adicionarUsuario');
			}
		}
		
	}

	public function addCliente(){
		$nome = $this->input->post("nome");
		$placa = $this->input->post("placa");
		$telefone = $this->input->post("telefone");
		$this->estacionamento->insertCliente($nome, $placa, $telefone);
		echo "<h2>Registro realizado com sucesso!</h2>
			<a href='/Estacionamento'>Voltar</a>
		";
	}

	public function addRegistro(){
		$placa = $this->input->post("placa");
		$cliente = $this->input->post("cliente");
		$this->estacionamento->adicionarEntrada($placa, $cliente);
		$registros = $this->estacionamento->getRegistros();
		echo"
			<table class='table'>
        <thead class='thead-dark'>
          <tr>
            <th scope='col'>Nome</th>
            <th scope='col'>Placa</th>
            <th scope='col'>Data de Entrada</th>
            <th scope='col'>Hora da entrada</th>
            <th scope='col'>Opções</th>
          </tr>
        </thead>
        <tbody>";
        foreach ($registros as $registro) {
        	echo "<tr>
		            <td>".$registro->cliente."</td>
		            <td>".$registro->placa."</td>
		            <td>".date('d/m/Y', strtotime($registro->dataDeEntrada))."</td>
		            <td>".$registro->horaDeEntrada."</td>
		            <td> <a class='btn btn-block btn-lg btn-danger text-light' id='registrarSaida' href='/Estacionamento/registrarSaida/".$registro->id."'>Registrar Saída</a> </td> 
		          </tr>
		        ";
        }
        echo"</tbody>
		      </table>";
   
	}

	public function pesquisarPlaca(){
		$placa = $this->input->post("placa");
		$dados["dados"] = $this->estacionamento->PesquisarPlaca($placa);
		$this->load->view('pesquisaCliente',$dados); 
	}


	public function deletaCliente($id){
		$this->estacionamento->deletaCliente($id);
		header("location:/Estacionamento/verificaCliente");
	}


	public function alteraCliente($id){
		$dados["id"] = $id;
		$this->load->view('atualizaCliente',$dados);
	}



	public function updateCliente(){
		$nome = $this->input->post("nome");
		$placa = $this->input->post("placa");
		$telefone= $this->input->post("telefone");
		$id = $this->input->post("id");
		$this->estacionamento->updateCliente($nome,$placa,$telefone,$id);
		header("location:/Estacionamento/verificaCliente");
	}


	//Funcoes de carregar VIEWS

	public function pesquisaCliente(){
		$this->load->view('pesquisaCliente');

	}

	public function cadastrarCliente(){
		$this->load->view("cadastrarCliente");
	}

	public function verificaCliente(){
		$this->load->view("verificaClientes");
	}
	

	public function adicionarUsuario(){
		if(isset($_SESSION['nome'])){
			$this->load->view('adicionarUsuario');
		}else{
			header('location: /Estacionamento');		
		}
	}
}