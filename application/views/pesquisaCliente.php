<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">

    <title>Novo usuário</title>
    <style type="text/css">
      .dropdown:hover>.dropdown-menu {
        display: block;
      }
    </style>
  </head>
  <body>
    <!--Carrega a barra superior-->
    <?php $this->load->view('header');?>
    <div class="row" style="margin: 2%">
      <div class="col-md-4">
        <div class="card" style="padding: 3%">
              <form action="pesquisarPlaca" method="post">
                <div class="form-group">
                  <label for="nome">Placa do Carro</label>
                  <input type="text" name="placa" class="form-control" id="placa" placeholder="Digite a placa do carro">
                </div>

                <button type="submit" class="btn btn-primary float-md-right " >Pesquisar</button> <a class="btn btn-danger float-md-right" style="margin-right: 15px" href="/Estacionamento">Cancelar</a> 
              </form>
            </div>
      </div>
      <div class="col-md-4">
      </div>
    </div>


       <table class='table' style="margin-top: 15px">
            <thead class='thead-dark'>
              <tr>
                <th scope='col'>Placa</th>
                <th scope='col'>Data de Entrada</th>
                <th scope='col'>Hora da Entrada</th>
                <th scope='col'>Hora da Saida</th>
                <th scope='col'>Valor</th>
              </tr>
            </thead>
             <tbody>
               <?php 

                $valortotal=0;
                if(isset($dados)){

                  foreach($dados as $dado){

                    echo "<tr>

                          <td>".$dado->placa."</td>
                          <td>".date('d/m/Y', strtotime($dado->dataDeEntrada))."</td>
                          <td>".$dado->horarioDeEntrada."</td>
                          <td>".$dado->horarioDeSaida."</td>
                          <td>".$dado->valor."</td>
                          </tr>
                    ";
                    $valortotal += $dado->valor;
                  }
                }

                ?>
             </tbody>
        </table>

    <div class="row" style="margin: 2%">
      <div class="col-md-2">
          <div class="card" style="padding: 3%">
              <form action="" method="post">
                <div class="form-group">
                  <label for="nome">Valor Total</label>
                  <input type="text" name="valor" class="form-control" id="valor" readonly="true" value="<?php echo $valortotal ?>">
                </div>
              </form>
          </div>
      </div>
    </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>