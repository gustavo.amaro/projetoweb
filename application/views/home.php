<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  date_default_timezone_set("America/Sao_Paulo");
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <style type="text/css">
      .dropdown:hover>.dropdown-menu {
        display: block;
      }
    </style>

    <title>Estacionamento</title>
  </head>
  <body>
    <!--Carrega a barra superior-->
    <?php $this->load->view('header')?>
    <!--Botões de entrada e saída de veículos-->
    <div class="container">
      <div class="row" style="margin-top: 1%">
        <a class="btn btn-block btn-lg btn-primary text-light" id="registrarEntrada" data-toggle="modal" data-target="#modalEntrada">Registrar Entrada</a>
      </div>
    </div>

    <div class="container" id="tabela" style="margin-top: 2%">
      <?php
        $registros = $this->estacionamento->getRegistros();
        echo"
          <table class='table'>
            <thead class='thead-dark'>
              <tr>
                <th scope='col'>Nome</th>
                <th scope='col'>Placa</th>
                <th scope='col'>Data de Entrada</th>
                <th scope='col'>Hora da entrada</th>
                <th scope='col'>Opções</th>
              </tr>
            </thead>
            <tbody>";
            foreach ($registros as $registro) {
              echo "<tr>
                    <td>".$registro->cliente."</td>
                    <td>".$registro->placa."</td>
                    <td>".date('d/m/Y', strtotime($registro->dataDeEntrada))."</td>
                    <td>".$registro->horaDeEntrada."</td>
                    <td> <a class='btn btn-block btn-lg btn-danger text-light' id='registrarSaida' href='/Estacionamento/registrarSaida/".$registro->id."'>Registrar Saída</a> </td> 
                  </tr>"
                ;
            }
            echo"</tbody>
              </table>";
      ?>

    </div>
    

    <div class="modal fade" id="modalEntrada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Registrar Entrada</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="entrada" method="POST">
              <div class="form-group">
                <label for="placa" class="col-form-label">Placa: </label>
                <input type="text" class="form-control" id="placa" name="placa">
              </div>

              <div class="form-group">
                <label for="placa" class="col-form-label">Cliente: </label>
                <input type="text" class="form-control" id="cliente" name="cliente" placeholder="(Opcional)">
              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="fechar" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="button" id="registrar" class="btn btn-primary">Registrar</button>
          </div>
        </div>
      </div>
    </div>
         
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $("#modalEntrada").ready(function(){
        $("#registrar").click(function(){
          var dados = $("#entrada").serialize();
          $.ajax({
            url: '/Estacionamento/addRegistro',
            type: 'POST',
            dataType: 'html',
            data: dados,
            success: function(resultado){
              $("#tabela").html(resultado);
              $("#modalEntrada").modal("hide");
            }
          });
          return false;
        });
      });
    </script>
  </body>
</html>