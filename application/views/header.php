<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="\Estacionamento">Estaciona System</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/Estacionamento">Home</span></a>
      </li>

      <div class="dropdown show">
        <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastro
        </a>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="/Estacionamento/adicionarUsuario">Adicionar usuário</a>
          <a class="dropdown-item" href="/Estacionamento/cadastrarCliente">Cadastrar Cliente</a>
        </div>
      </div>

      <li>
        <a class="nav-link" href="/Estacionamento/verificaCliente"><span>Clientes Cadastrados</span></a>
      </li>

      <li>
          <a  class="nav-link" href="/Estacionamento/pesquisaCliente"><span>Situação do Cliente</span></a>
        </li>
    </ul>
  </div>
  <span class="text-uppercase text-light" style="margin-right: 15px">Olá, <?php echo $_SESSION['nome'];?>!</span> <a href="sair" class="btn btn-danger">Sair</a>
</nav>