<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">

    <title>Entrar</title>
  </head>
  <body style="background: url('http://st.gde-fon.com/wallpapers_original/535128_dojd_ulitsa_gorod_mashinyi_1920x1080_www.Gde-Fon.com.jpg') no-repeat; background-size: 100%">

    <div class="container-fluid">
      
      <div class="row" style="margin-top: 10%;">

        <div class="col-md-6">
          <h1 class="text-white text-center" style="padding: 5px">Utilize o usuário e senha cadastrados para acessar o sistema.</h3>
        </div>

        <div class="col-md-4">
          <div class="card" style="padding: 3%">
            <?php if(isset($_SESSION['erro'])){ echo "<p class='text-danger'>". $_SESSION['erro']."</p>"; unset($_SESSION['erro']); }?>
            <form action="home" method="post">

              <div class="form-group">
                <label for="email">Usuário</label>
                <input type="text" name="usuario" class="form-control" id="usuario" aria-describedby="emailHelp" placeholder="Digite seu nome de usuário">
              </div>

              <div class="form-group">
                <label for="senha">Senha</label>
                <input type="password" name="senha" class="form-control" placeholder="Senha" id="senha">
              </div>

              <button type="submit" class="btn btn-primary btn-block">Entrar</button>

            </form>
          </div>
        </div>
      </div>

    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    </script>
  </body>
</html>