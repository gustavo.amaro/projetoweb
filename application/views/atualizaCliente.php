<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <title>Altera Usuario</title>
    <style type="text/css">
      .dropdown:hover>.dropdown-menu {
        display: block;
      }
    </style>
  </head>
  <body>
    <?php  $cliente = $this->estacionamento->getUnicoCliente($id); echo $id?>
    <!--Carrega a barra superior-->
    <div class="row" style="margin: 2%">
    <div class="col-md-4"></div>
    <div class="col-md-4">
          <div class="card" style="padding: 3%">
            <form action="/Estacionamento/updateCliente" method="post">
              <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite o nome do cliente" value="<?php echo $cliente->nome;?>">
              </div>

              <div class="form-group">
                <label for="placaId">Placa</label>
                <input type="text" name="placa" class="form-control" id="placaId"  placeholder="Digite a placa do carro" value="<?php echo $cliente->placa;?>">
              </div>

              <div class="form-group">
                <label for="telefoneId">Telefone</label>
                <input type="text" name="telefone" class="form-control" placeholder="Digite o telefone do cliente" id="telefoneId" value="<?php echo $cliente->telefone;?>">
              </div>

              <div class="form-group">
                <input type="text" name="id"  style="display: none;"class="form-control" id="id" value="<?php echo $cliente->id;?>">
              </div>

              <button type="submit" class="btn btn-primary float-md-right " >Alterar</button> <a class="btn btn-danger float-md-right" style="margin-right: 15px" href="/Estacionamento">Cancelar</a> 
            </form>
        </div>
      </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>