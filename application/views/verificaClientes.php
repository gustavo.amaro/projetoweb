<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">

    <title>Novo usuário</title>
    <style type="text/css">
      .dropdown:hover>.dropdown-menu {
        display: block;
      }
    </style>
  </head>
  <body>
    <!--Carrega a barra superior-->
    <?php $this->load->view('header');
        $clientes = $this->estacionamento->getClientes();
    ?>
    <div clas="row" style="margin: 2%">
           <table class='table' style="margin-top: 15px">
                <thead class='thead-dark'>
                  <tr>
                    <th scope='col'>Nome</th>
                    <th scope='col'>Telefone</th>
                    <th scope='col'>Placa</th>
                    <th scope='col'>Opcoes</th>
                  </tr>
                </thead>
                 <tbody>

                  <?php 

                    foreach ($clientes as $cliente) {
                      echo "
                        <tr>

                              <td>".$cliente->nome."</td>
                              <td>".$cliente->telefone."</td>
                              <td>".$cliente->placa."</td>
                              <td> <a class='btn btn-lg btn-primary text-light' id='AlterarCliente' href='/Estacionamento/AlteraCliente/".$cliente->id."'>Alterar</a> <a class='btn btn-lg btn-danger text-light' id='ExcluirCliente' href='/Estacionamento/deletaCliente/".$cliente->id."'>Excluir</a> </td> 
                              </tr>
                      ";
                    }

                  ?>

                 </tbody>
            </table>
      </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
  </body>
</html>