<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  date_default_timezone_set("America/Sao_Paulo");
?>
<!doctype html>
<html lang="pt-br">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <title>Estacionamento</title>
  </head>
  <body>
    <div class="container-fluid" style="margin-top: 2%; ">
      <div class="row" style="margin-top: 5%">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"> 
          <p>Placa: <?php echo $placa; ?></p>
          <p> Hora de entrada: <?php echo $horaDeEntrada; ?> </p>
          <p>Hora de Saída: <?php echo $horaDeSaida; ?> </p>
          <p>Data: <?php echo date('d/m/Y', strtotime($dataDeEntrada)); ?></p> 
          <p>Valor: <?php echo "R$".number_format($valor,2,",","."); ?> </p>
          <a class="btn btn-danger" href="/Estacionamento">Cancelar</a>
          <a class="btn btn-primary" href="/Estacionamento/confirmaSaida/<?php echo $id ?>/<?php echo $placa ?>/<?php echo $valor ?>/<?php echo $horaDeEntrada ?>/<?php echo $horaDeSaida ?>/<?php echo $dataDeEntrada ?>">Confirmar</a>

        </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
  </body>

</html>